/*
 * 
 */
package dev181.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;

/**
 * Excel -> CSV変換<BR>
 * 数式にも対応（計算結果をそのまま取得します）
 *
 * @author 181dev
 */
public class ExcelReader {

    public static void main(String[] args) {
        new ExcelReader().excelToCsv("geppo.xls");
    }

    /**
     * xlsシートの読み込み
     *
     * @param file
     * @throws java.io.IOException
     */
    public ArrayList<ArrayList<String>> read(File file) throws IOException {
        ArrayList<ArrayList<String>> data;
        POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
        HSSFWorkbook workbook = new HSSFWorkbook(fs);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator iter = sheet.rowIterator();

        HSSFRow r;
        data = new ArrayList();
        while (iter.hasNext()) {
            r = (HSSFRow) iter.next();
            if (r == null) {
                break;
            } else {
                data.add(toList(r));
            }
        }

        return data;
    }

    /**
     * xlsシートからデータ配列に変換
     *
     * @param row
     * @return
     */
    private ArrayList<String> toList(HSSFRow row) {
        ArrayList<String> vector = new ArrayList<>();
        Iterator cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            HSSFCell cell = (HSSFCell) cellIterator.next();
            int cellType = cell.getCellType();
            String str = "";
            if (cellType == Cell.CELL_TYPE_NUMERIC) {
                str = "" + cell.getNumericCellValue();
            } else if (cellType == Cell.CELL_TYPE_FORMULA) {//セルが数式
                //Excelはセルの計算内容をキャッシュしている。
                //そのためPOI経由でも計算結果が取得できる。
                //ただし、POIから書き換えを行ったときは、Excelでファイルを開いて再計算を行う必要がある。
                switch (cell.getCachedFormulaResultType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        str = "" + cell.getNumericCellValue();
                        break;
                    case Cell.CELL_TYPE_STRING:
                        str = cell.getRichStringCellValue().getString();
                        break;
                }
            } else {
                str = cell.getRichStringCellValue().getString();
            }
            vector.add(str);

        }
        return vector;
    }

    private void excelToCsv(String fileName) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        try {
            fos = new FileOutputStream("out.csv");
            osw = new OutputStreamWriter(fos, "SJIS");
            ArrayList<ArrayList<String>> read = read(new File(fileName));
            for (ArrayList<String> row : read) {
                //使うときには、CSV出力をちゃんと実装しなおしてね（セル内改行やエスケープに未対応）
                osw.write(StringUtils.join(row, ", ") + "\n");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                osw.close();
            } catch (IOException ex) {
                Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
